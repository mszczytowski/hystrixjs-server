var express = require('express'),
    hystrix = require('hystrixjs');

var app = express();

var configs = {

    sport: {
        name: 'sport',
        hostname: 'api.snd.no',
        port: 80,
        path: '/sport/api/',
        cacheMaxAge: 60000,
        fallback: function(req, callback) {
            callback(null, '{}');
        },
        processor: function(message, callback) {
            callback(null, JSON.stringify(JSON.parse(message).collection.items[0].data));
        }
    }

}

app.all('/sport/*', hystrix(configs['sport']));

app.listen(3000);